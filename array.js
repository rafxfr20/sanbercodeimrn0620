//Soal 1 Array
console.log('Soal 1 Array')

function range(startNum, finishNum) {
    var number = []

    if (!startNum || !finishNum) {
        return -1
    }
    else if (startNum < finishNum) {
        for (i = startNum; i <= finishNum; i++) {
            number.push(i)
        }
        return number
    }
    else {
        for (i = startNum; i >= finishNum; i--) {
            number.push(i)
        }
        return number
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log('\n')

//Soal 2 Array
console.log('Soal 2 Array')

function rangeWithStep(startNum, finishNum, step) {
    var number = []

    if (startNum < finishNum) {
        for (i = startNum; i <= finishNum; i = i + step) {
            number.push(i)
        }
    }
    else {
        for (i = startNum; i >= finishNum; i = i - step) {
            number.push(i)
        }
    }

    return number
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log('\n')

//Soal 3 Array
console.log('Soal 3 Array')

function sum(startNum, finishNum, step) {
    var number = 0
    var arr
    var i

    if (!startNum && !finishNum && !step) {
        number = 0
    }
    else if (startNum && !finishNum && !step) {
        number = startNum
    }
    else {
        if (!step) {
            arr = rangeWithStep(startNum, finishNum, 1)
        }
        else {
            arr = rangeWithStep(startNum, finishNum, step)
        }
        for (i = 0; i < arr.length; i++) {
            number = number + parseInt(arr[i])
        }
    }

    return number
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log('\n')

//Soal 4 Array
console.log('Soal 4 Array')

function dataHandling(arr) {
    var i

    for (i = 0; i < arr.length; i++) {
        console.log(`Nomor ID: ${arr[i][0]}`)
        console.log(`Nama Lengkap: ${arr[i][1]}`)
        console.log(`TTL: ${arr[i][2]} ${arr[i][3]}`)
        console.log(`Hobi: ${arr[i][4]}`)
        console.log()
    }
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

dataHandling(input)

console.log('\n')

//Soal 5 Array
console.log('Soal 5 Array')

function balikKata(input) {
    var i
    var kata = ''

    for (i = input.length - 1; i >= 0; i--) {
        kata = kata + input[i]
    }

    return kata
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('\n')

//Soal 6 Array
console.log('Soal 6 Array')

function dataHandling2(arr) {
    var tanggal = []
    var bulan
    var parseBulan
    var stringNama


    // Splice
    arr.splice(1, 1, `${arr[1]} Elsharawy`)
    arr.splice(2, 1, `Provinsi ${arr[2]}`)
    arr.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(arr)


    // Split
    tanggal = arr[3].split("/")
    bulan = parseInt(tanggal[1])
    switch (bulan) {
        case 1: {
            parseBulan = "Januari";
            break;
        }
        case 2: {
            parseBulan = "Februari";
            break;
        }
        case 3: {
            parseBulan = "Maret";
            break;
        }
        case 4: {
            parseBulan = "April";
            break;
        }
        case 5: {
            parseBulan = "Mei";
            break;
        }
        case 6: {
            parseBulan = "Juni";
            break;
        }
        case 7: {
            parseBulan = "Juli";
            break;
        }
        case 8: {
            parseBulan = "Agustus";
            break;
        }
        case 9: {
            parseBulan = "September";
            break;
        }
        case 10: {
            parseBulan = "Oktober";
            break;
        }
        case 11: {
            parseBulan = "November";
            break;
        }
        case 12: {
            parseBulan = "Desember";
            break;
        }
        default: {
            parseBulan = "";
        }
    }
    console.log(parseBulan)

    // Sorting
    tanggal.sort(
        function (a, b) {
            return b - a
        }
    )
    console.log(tanggal)

    // Join
    tanggal = (arr[3].split("/")).join("-")
    console.log(tanggal)
    
    // Slice
    stringNama = String(arr[1])
    console.log(stringNama.slice(0,15))
}

input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)