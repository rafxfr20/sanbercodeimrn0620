console.log("Soal 1 002")
function DescendingTen(num){
    for(num;num>0;num--){
        console.log(num)
        if(num==91){
            break;
        }else
        if(!num){
            return -1
        }
    }
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log('\n')

console.log("Soal 2 002")
function AscendingTen(num){
    for(num;num<110;num++){
        console.log(num)
        if(num==20){
            break;
        }else
        if(num==30){
            break;
        }else
        if(!num){
            return -1
        }
    }
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log('\n')

console.log("Soal 2 002")
function ConditionalAscDesc(num1,num2){
    if(num2%2==0){
        if(num1==20){
            for(num1;num1>10;num1--){
                console.log(num1)
        }
    }
    }else 
    if(num2%2==1){
        if(num1==81){
            for(num1;num1<90;num1++){
                console.log(num1)
        }
    }
}
else{
    return -1
}
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1