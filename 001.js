console.log("Soal 1 001")
function bandingkan(number1, number2){
    if(number1==number2 || number1<0 || number2<0){
        return -1
    }else
    if(number1<number2){
        return number2
    }else
    if(number1>number2){
        return number1
    }else
    if(number1>0 && !number2){
        return number1
    }else
    if(number2>0 && !number1){
        return number2
    }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18

console.log('\n')

console.log("Soal 2 001")
function balikString(input) {
    var i
    var kata = ''

    for (i = input.length - 1; i >= 0; i--) {
        kata = kata + input[i]
    }
    return kata
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log('\n')

console.log("Soal 3 001")
function palindrome(input) {
    var i
    var kata = ''

    for (i = input.length - 1; i >= 0; i--) {
        kata = kata + input[i]
    }
    if(input==kata){
        console.log('true')
    }else{
        console.log('false')
    }
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false