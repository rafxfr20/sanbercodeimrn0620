//Soal 1 Functions
console.log('Soal 1')
function teriak() {
    return "Halo Sanbers!" 
}
console.log(teriak())

console.log('\n')

//Soal 2 Functions
console.log('Soal 2')
function kalikan(num1, num2) {
    return num1*num2 
}
var hasilKali = kalikan(12,4)
console.log(hasilKali)

console.log('\n')

//Soal 3 Functions
console.log('Soal 3')
function introduce(name, age, address, hobby) {
    return "Nama saya "+name+", umur saya "+age+" tahun"+", alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!"
}
var perkenalan = introduce("Agus",30,"Jln. Malioboro, Yogyakarta","Gaming")
console.log(perkenalan)