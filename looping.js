//Soal 1 Looping (WHILE)
console.log('Soal 1 Looping')
var num = 2;
console.log('LOOPING PERTAMA')
while(num < 21) { 
  console.log(num + ' - I love coding'); // Menampilkan nilai flag pada iterasi tertentu
  num+=2; // Mengubah nilai flag dengan menambahkan 1
}
console.log('LOOPING KEDUA')
if(num==22){
    num = 20;
    while(num > 1) { 
        console.log(num + ' - I will become a mobile developer'); // Menampilkan nilai flag pada iterasi tertentu
        num-=2; // Mengubah nilai flag dengan menambahkan 1
    }
}

console.log('\n')

//Soal 2 Looping (FOR)
console.log('Soal 2 Looping')
var num = 1;
for(num; num<21; num++){
    if(num%2==0){
        console.log(num + ' - Berkualitas')
    }else
    if(num%3==0){
        console.log(num +' - I love coding')
    }else{
        console.log(num +' - Santai')
    }
}

console.log('\n')

//Soal 3 Looping (FOR)
console.log('Soal 3 Looping')
var lebar = 8;
var panjang = 4
var result;
for (var a=1; a<=panjang; a++){
        result = '';
    for(var b=1; b<=lebar; b++){
        result += '#';  
    }
    console.log(result);
}

console.log('\n')

//Soal 4 Looping (FOR)
console.log('Soal 4 Looping')
var panjang = 7
var result;
for (var a=1; a<=panjang; a++){
        result = '';
    for(var b=1; b<a; b++){
        result += '#';
}
    console.log(result);
}

console.log('\n')

//Soal 5 Looping (FOR)
console.log('Soal 5 Looping')
for (let kotak1 = 0; kotak1 < 8; kotak1++) {
    let start = kotak1 % 2 === 1 ? ' ' : '#';
    let hasil = '';
    for (let kotak2 = 0; kotak2 < 8; kotak2++) {
        start = start == '#' ? ' ' : '#';
        hasil += start;
    }
    console.log(hasil);
}